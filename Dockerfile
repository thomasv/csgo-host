FROM ubuntu:14.04
MAINTAINER thomas@vendetta.io

RUN apt-get update
RUN apt-get install -y \
      gdb \
      lib32gcc1 \
      wget \
      unzip \
      rsync

RUN adduser --disabled-password --gecos "" server

WORKDIR /home/server

# Download and install steamcmd
RUN mkdir /home/server/steamcmd &&\
      cd /home/server/steamcmd &&\
      wget https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz &&\
      tar -xvzf steamcmd_linux.tar.gz

RUN chown -R server:server ./

USER server

ADD util /home/server/util

#RUN /bin/bash -C "/home/server/util/scripts/install_cs.sh"

EXPOSE 27915
EXPOSE 27915/udp
