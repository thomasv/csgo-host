#!/bin/bash
#
# Host.sh - Thomas Vendetta - 08/06/15
# 
# Provides set of helper functions for a host machine running docker:
# builds a Docker image 'game' which installs SteamCMD and dedicated
# source servers (right now only CS:GO) on a host machine.
#
# Both ./data/ and ./util/ get mounted to /home/server when a container 
# is launched. 
# 

if [ -f ".csgohost" ]; then
  source .csgohost
fi

printUsage() {
  echo "host.sh"
  echo "-------"
  echo "./host.sh test"
  echo "          - Runs test container with all data and util dirs mounted"
  echo "./host.sh install-csgo"
  echo "          - Downloads and installs CSGO to the hosts' data directory"
  echo "./host.sh update-csgo"
  echo "          - Updates CSGO"
  echo "./host.sh install-mods"
  echo "          - Downloads and installs Sourcemod and Meta mod to the host data directory"
}

# $HOST_DIR defaults to $HOME/csgo-host/current
# $HOST_DIR should be set to the parent folder of host.sh, 
# Mina is used to deploy code to host machines so we default to /current
if [ -z $HOST_DIR ]; then
  HOST_DIR="$HOME/csgo-host/current"
  echo "HOST_DIR defaulting to: $HOST_DIR"
else
  echo "HOST_DIR set by .csgohost: $HOST_DIR"
fi

if [ -z $DATA_DIR ]; then
  DATA_DIR="$HOST_DIR/data"
  echo "DATA_DIR defaulting to: $DATA_DIR"
fi

# Check to see if the directory exists
if [ ! -d $DATA_DIR ]; then
  mkdir -p $DATA_DIR
  echo "$DATA_DIR did not exist, created."
fi

mkdir -p "$DATA_DIR/cfgs"

if [ "$1" != "" ]; then
  echo "Host.sh executed"
  echo "----------------"
  echo "Host Dir: $HOST_DIR"
  echo "Data Dir: $DATA_DIR"
  echo "Util Dir: $UTIL_DIR"
  echo "Executing: $1"
  echo "----------------"
fi

if [ "$1" == "test" ]; then
  docker run \
    -t \
    --rm \
    -i \
    -v "$DATA_DIR:/home/server/data" \
    -P \
    game \
    /bin/bash
elif [ "$1" == "build" ]; then
  IMAGE_NAME="game"
  docker build -t $IMAGE_NAME .
elif [ "$1" == "install-csgo" ]; then
  docker run \
    --rm \
    -v "$DATA_DIR:/home/server/data" \
    game \
    /bin/bash -C "/home/server/util/scripts/install_cs.sh"
elif [ "$1" == "update-csgo" ]; then
  docker run \
    --rm \
    -v "$DATA_DIR:/home/server/data" \
    game \
    /bin/bash -C "/home/server/util/scripts/update-cs.sh"
elif [ "$1" == "install-mods" ]; then
  docker run \
    --rm \
    -v "$DATA_DIR:/home/server/data" \
    game \
    /bin/bash -C "/home/server/util/scripts/install-mods.sh"
else 
  printUsage
fi
