SOURCEMOD_URL="http://cdn.probablyaserver.com/sourcemod/sourcemod-1.7.2-linux.tar.gz"
SOURCEMOD_TAR_FILE="sourcemod-1.7.2-linux.tar.gz"
METAMOD_URL="http://cdn.probablyaserver.com/sourcemod/mmsource-1.10.5-linux.tar.gz"
METAMOD_TAR_FILE="mmsource-1.10.5-linux.tar.gz"
PUGSETUP_URL=https://github.com/splewis/csgo-pug-setup/releases/download/1.4.2/pugsetup_1.4.2.zip  
PUGSETUP_ZIP_FILE=pugsetup_1.4.2.zip
RETAKES_ZIP_FILE=retakes_0.2.0.zip
RETAKES_URL=https://github.com/splewis/csgo-retakes/releases/download/v0.2.0/retakes_0.2.0.zip


MODS_DIR=/home/server/data/mods

mkdir -p $MODS_DIR

cd $MODS_DIR

echo ""
if [ ! -f "$MODS_DIR/$SOURCEMOD_TAR_FILE" ]; then
  wget $SOURCEMOD_URL
else
  echo " -- Already have sourcemod."
fi

if [ ! -f "$MODS_DIR/$METAMOD_TAR_FILE" ]; then
  wget $METAMOD_URL
else
  echo " -- Already have metamod."
fi

mkdir $MODS_DIR/sm_mm_base

tar -xzvf $SOURCEMOD_TAR_FILE -C ./sm_mm_base
tar -xzvf $METAMOD_TAR_FILE -C ./sm_mm_base

wget $PUGSETUP_URL
wget $RETAKES_URL

unzip $PUGSETUP_ZIP_FILE -d pugsetup
unzip $RETAKES_ZIP_FILE -d retakes

echo "Enabling practicemode by default"
mv $MODS_DIR/pugsetup/addons/sourcemod/plugins/disabled/pugsetup_practicemode.smx \
   $MODS_DIR/pugsetup/addons/sourcemod/plugins/

mkdir -p $MODS_DIR/dl

mv $MODS_DIR/*.zip $MODS_DIR/dl
mv $MODS_DIR/*.tar.gz $MODS_DIR/dl
