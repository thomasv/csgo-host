#!/bin/bash
# Creates a server specific game install folder inside the container,
# symlinks all the items from the game install on the host machine,
# remove the ones we need to modify for specific configurations or mods,
# and then copy them over into the container/server specific game install.
# 
# Expects $SRCDS_RUN_ARGUMENTS, $GAMEMODE, $SERVER_ID, $PORT

echo " -- Launching CS:GO Server --"
echo " -- Server ID: $SERVER_ID"
echo " -- GameMode: $GAMEMODE"
echo " -- Run Arguments: $SRCDS_RUN_ARGUMENTS"

SERVER_PATH="$HOME/server_$SERVER_ID"
DATA_CSDS_DIR="$HOME/data/csgo_ds"
DATA_CSGO_DIR="$DATA_CSDS_DIR/csgo"

echo "Making $SERVER_PATH"
mkdir -p $SERVER_PATH

# Copy all of csgo_ds/
ln -s $DATA_CSDS_DIR/* $SERVER_PATH

# Remove /csgo folder symlink
echo " -- Removing symlink for $SERVER_PATH/csgo"
rm "$SERVER_PATH/csgo"

echo " -- Making new $SERVER_PATH/csgo directory"
mkdir -p $SERVER_PATH/csgo

# Symlink all of original csgo into new server path
ln -s $DATA_CSGO_DIR/* $SERVER_PATH/csgo

# Remove $SERVER_PATH/csgo/cfg and $SERVER_PATH/csgo/addons
rm $SERVER_PATH/csgo/cfg
rm $SERVER_PATH/csgo/addons
mkdir -p $SERVER_PATH/csgo/cfg $SERVER_PATH/csgo/addons

#echo "Copying any cfg from data to server"
cp -R $DATA_CSGO_DIR/cfg/* $SERVER_PATH/csgo/cfg
#echo "Copying any addons from data to server"
cp -R $DATA_CSGO_DIR/addons/* $SERVER_PATH/csgo/addons

#cp /home/server/data/cfgs/$SERVER_ID-autoexec.cfg $SERVER_PATH/csgo/cfg/autoexec.cfg
cp /home/server/data/cfgs/$SERVER_ID-server.cfg $SERVER_PATH/csgo/cfg/server.cfg

echo " -- Installing sourcemod and meta mod base"
rsync -a /home/server/data/mods/sm_mm_base/addons/* $SERVER_PATH/csgo/addons/
rsync -a /home/server/data/mods/sm_mm_base/cfg/* $SERVER_PATH/csgo/cfg/

echo " -- Copying Default MOTD/AutoExec/Server configs"
cp /home/server/util/motd.txt $SERVER_PATH/csgo/motd.txt 

if [ $GAMEMODE == "Casual" ]; then
  echo " -- Setting casual mode configuration"
  #  /home/server/server_$SERVER_ID/srcds_run $SRCDS_RUN_ARGUMENTS

elif [ $GAMEMODE == "Impact" ]; then
  echo " -- Setting nade/impact mode configuration"
  rsync -a /home/server/data/mods/pugsetup/* $SERVER_PATH/csgo/

  echo " -- Copying MOTD/AutoExec/Server configs"
  cp /home/server/util/motd-impact.txt $SERVER_PATH/csgo/motd.txt 

  cp -v /home/server/util/practice_gamemodes_server.txt $SERVER_PATH/csgo/gamemodes_server.txt
  
  cp -v /home/server/util/practice.cfg $SERVER_PATH/csgo/cfg/practice.cfg

  echo " -- Installed nade/impact mode. Starting server."
  /home/server/server_$SERVER_ID/srcds_run $SRCDS_RUN_ARGUMENTS

elif [ $GAMEMODE == "Retakes" ]; then
  echo " -- Setting retake mode configuration"
  rsync -a /home/server/data/mods/retakes/* $SERVER_PATH/csgo/
  echo " -- Copying MOTD/AutoExec/Server configs"
  cp /home/server/util/motd-retake.txt $SERVER_PATH/csgo/motd.txt 
  cp /home/data/mods/csgo_gunmenu.smx $DATA_CSGO_DIR/addons/sourcemod/plugins/
  echo " -- Installed retake mode. Starting server."
  /home/server/server_$SERVER_ID/srcds_run $SRCDS_RUN_ARGUMENTS
else
  echo " -- Setting competitive pug mode configuration"
  rsync -a /home/server/data/mods/pugsetup/* $SERVER_PATH/csgo/

  echo " -- Copying MOTD/AutoExec/Server configs"
  cp /home/server/util/motd-competitive.txt $SERVER_PATH/csgo/motd.txt 

  #  /home/server/server_$SERVER_ID/srcds_run $SRCDS_RUN_ARGUMENTS

fi

RUNCMD="$SERVER_PATH/srcds_run $SRCDS_RUN_ARGUMENTS"
echo "Running this cmd: $RUNCMD"
sh -C $RUNCMD
#/bin/bash
#/bin/bash
#sh -C "$SERVER_PATH/srcsds_run $SRCDS_RUN_ARGUMENTS"
